import {atom} from "recoil";

export const addToCartDetails = atom({
    key: "addToCartDetails",
    default: [],
});
