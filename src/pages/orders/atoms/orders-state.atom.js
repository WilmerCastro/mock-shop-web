import {atom} from "recoil";

export const ordersState = atom({
    key: 'ordersState',
    default: {},
});

export const totalToPayState = atom({
    key: 'totalToPayState',
    default: {},
});
