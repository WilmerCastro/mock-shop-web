import {atom} from "recoil";

export const productDetailState = atom({
    key: "productDetailState",
    default: [],
});
